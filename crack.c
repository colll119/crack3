#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char plain[33];
    char hash[50];
};
// sorts the dictionary by hash
int byHash(const void *a, const void *b)
{
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    return strcmp(aa->hash,bb->hash);
}
// searches the dictionary for a specific hash
int hashSearch(const void *d, const void *l)
{
    char * dd = (char *)d;
    struct entry *ll = (struct entry *)l;
    return strcmp(dd,ll->hash);
}
// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    FILE *f = fopen(filename,"r");
    if(!f)
    {
        char err[100];
        sprintf(err,"Can't open %s for reading",filename);
        perror(err);
        exit(1);
    }
    
    int capacity = 100;
    int curr = 0;
    struct entry *contents=malloc(capacity * sizeof(struct entry));
    char line[100];
    while(fgets(line,100,f) != NULL)
    {
        if(curr >= capacity)
        {
            capacity +=10;
            contents = realloc(contents,capacity * sizeof(struct entry));
        }
        sscanf(line, "%s",contents[curr].plain);
        char *nl = strchr(line,'\n');
        if(nl)
        {
            *nl = '\0';
        }
        char *hash = md5(line,strlen(line));
        strcpy(contents[curr].hash,hash);
        free(hash);
        curr++;
    }
    fclose(f);
    *size = curr;
    return contents;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int dlen;
    struct entry *dict = read_dictionary(argv[2], &dlen);

    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    qsort(dict, dlen, sizeof(struct entry), byHash);
    /*for(int i = 0; i < dlen; i++)
    {
        printf("Plaintext: %s, Hash: %s\n",dict[i].plain,dict[i].hash);
    }
    */
    // Partial example of using bsearch, for testing. Delete this line
    // once you get the remainder of the program working.
    // This is the hash for "rockyou".
    struct entry *found = bsearch("f806fc5a2a0d5ba2471600758452799c", dict, dlen, sizeof(struct entry), hashSearch);
        if(found)
        {
            printf("Found: %s, %s\n", found->plain, found->hash);
            
        }
    // TODO
    // Open the hash file for reading.
    FILE *h = fopen(argv[1],"r");
    if(!h)
    {
        char err[100];
        sprintf(err,"Can't open %s for reading\n",argv[1]);
        perror(err);
        exit(1);
    }
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
     char line[100];
    while(fgets(line,100,h) != NULL)
    {
        char *nl = strchr(line,'\n');
        if(nl)
        {
            *nl = '\0';
        }
        found = bsearch(line, dict, dlen, sizeof(struct entry), hashSearch);
        if(found)
        {
            printf("Found: %s, %s\n", found->plain, found->hash);
        }
    }
    free(dict);    
    fclose(h);
}
